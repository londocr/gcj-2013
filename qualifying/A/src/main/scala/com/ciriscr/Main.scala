package com.ciriscr

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 25/04/13
 * Time: 15:35
 */

object Main extends App {

  correr(args(0))

  def correr(input: String) {
    val out = new Writer("a-result.out")
    val boards = Reader.read(input)
//    val boards = Reader.read("Input sample")
    var i = 1
    for (b <- boards) {
//      println(b)
      val result = b.checkWin
      out.escribir("Case #%s: %s".format(i, result.toString))
      i += 1
    }
  }
}
