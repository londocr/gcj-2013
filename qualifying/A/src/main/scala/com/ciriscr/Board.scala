package com.ciriscr

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 25/04/13
 * Time: 13:58
 */

class Board(lines: Iterator[String]) extends WinChecker {

  protected lazy val board = processLines
  private val length = 4

  def processLines = lines.toList.take(length).map(_.toList)

  lazy val rows = getRows
  lazy val columns = getColumns

  protected def getRows = board
  protected def getColumns = board.map(_.toTraversable).transpose
  protected def getDiagonals =
    List((0 until length).map(i => rows(i)(i)).toList, (0 until length).map(i => rows(i)(length - i - 1)).toList)

  override def toString = board.map(_.mkString).mkString("\n")

}

trait WinChecker {
  protected val board: List[List[Char]]

  protected def getRows: List[List[Char]]
  protected def getColumns: List[List[Char]]
  protected def getDiagonals: List[List[Char]]

  def checkWin: WinStatus.Value = {
    val posibilities = (getRows ::: getColumns ::: getDiagonals).map(_.mkString)
//    println(posibilities.mkString("\n"))
//    println()
    if (posibilities.foldLeft(false)(_ || _.matches("""(X|T){4}""")))
      WinStatus.xWin
    else if (posibilities.foldLeft(false)(_ || _.matches("""(O|T){4}""")))
      WinStatus.oWin
    else if (posibilities.foldLeft(false)((a, b) => a || !b.contains(".")))
      WinStatus.draw
    else
      WinStatus.notFinish
  }
}

object WinStatus extends Enumeration {
  val xWin = Value("X won")
  val oWin = Value("O won")
  val draw = Value("Draw")
  val notFinish = Value("Game has not completed")
}